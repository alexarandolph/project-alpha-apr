from django.shortcuts import render, redirect
from tasks.forms import TaskForm, NotesForm
from tasks.models import Task
from django.contrib.auth.decorators import login_required

# Create your views here.


def create_task(request):
    if request.method == "POST":
        form = TaskForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect("show_my_tasks")
    else:
        form = TaskForm()
    context = {
        "form": form,
    }
    return render(request, "tasks/create.html", context)


@login_required
def show_my_tasks(request):
    tasks = Task.objects.filter(assignee=request.user)
    context = {
        "tasks": tasks,
    }
    return render(request, "tasks/list.html", context)


@login_required
def add_notes(request, id):
    task = Task.objects.get(id=id)
    form = NotesForm(request.POST or None, instance=task)
    if form.is_valid():
        task = form.save(False)
        task.assignee = request.user
        return redirect("show_my_tasks")
    else:
        form = NotesForm
    context = {
        "form": form,
    }
    return render(request, "tasks/notes.html", context)


@login_required
def update_task(request, id):
    list = Task.objects.get(id=id)
    if request.method == "POST":
        form = TaskForm(request.POST, instance=list)
        if form.is_valid():
            list = form.save(False)
            list.assignee = request.user
            list.save()
            return redirect("show_my_tasks")
    else:
        form = TaskForm(instance=list)
    context = {
        "form": form,
    }
    return render(request, "tasks/update.html", context)
