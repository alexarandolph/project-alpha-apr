from django import forms
from tasks.models import Task


class TaskForm(forms.ModelForm):
    class Meta:
        model = Task
        fields = (
            "project",
            "name",
            "start_date",
            "due_date",
            "assignee",
            "notes",
            "is_completed",
        )


class NotesForm(forms.ModelForm):
    class Meta:
        model = Task
        fields = ("notes",)
