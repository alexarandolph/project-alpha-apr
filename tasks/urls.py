from django.urls import path
from tasks.views import create_task, show_my_tasks, add_notes, update_task

urlpatterns = [
    path("create/", create_task, name="create_task"),
    path("mine/", show_my_tasks, name="show_my_tasks"),
    path("<int:id>/notes/", add_notes, name="add_notes"),
    path("<int:id>/update/", update_task, name="update_task"),
]
