from django.contrib import admin
from projects.models import Project


# Register your models here.
@admin.register(Project)
class ProjectAdmin(admin.ModelAdmin):
    pass


# @admin.register(Company)
# class CompanyAdmin(admin.ModelAdmin):
#     pass
