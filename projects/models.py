from django.db import models
from django.conf import settings


# Create your models here.
class Project(models.Model):
    name = models.CharField(max_length=200)
    description = models.TextField(db_column="description")
    owner = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        related_name="projects",
        on_delete=models.CASCADE,
        null=True,
    )
    # company = models.ForeignKey(
    #     "Company",
    #     related_name="company",
    #     on_delete=models.CASCADE,
    #     null=True,
    # )

    def __str__(self):
        return self.name


# class Company(models.Model):
#     company_name = models.CharField(max_length=255)
#     manager = models.CharField(max_length=255)
#     department = models.CharField(max_length=255)
#     collaborators = models.CharField(max_length=255, blank=True)

#     def __str__(self):
#         return self.company_name
