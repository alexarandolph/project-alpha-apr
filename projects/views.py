from django.shortcuts import render, redirect
from projects.models import Project
from django.contrib.auth.decorators import login_required
from projects.forms import ProjectForm


@login_required
def list_projects(request):
    projects = Project.objects.filter(owner=request.user)
    context = {
        "projects": projects,
    }
    return render(request, "projects/list.html", context)


@login_required
def show_project(request, id):
    project = Project.objects.get(id=id)
    context = {
        "project": project,
    }
    return render(request, "projects/detail.html", context)


@login_required
def create_project(request):
    if request.method == "POST":
        form = ProjectForm(request.POST)
        if form.is_valid():
            project = form.save(False)
            project.owner = request.user
            project.save()
            return redirect("list_projects")
    else:
        form = ProjectForm()
    context = {
        "form": form,
    }
    return render(request, "projects/create.html", context)


@login_required
def search_projects(request):
    q = request.GET["query"]
    projects = Project.objects.filter(name__icontains=q)
    context = {
        "projects": projects,
    }
    return render(request, "projects/list.html", context)


# @login_required
# def company_projects(request):
#     projects = Project.objects.filter(company_name=request.user)
#     context = {
#         "projects": projects,
#     }
#     return render(request, "projects/list.html", context)


@login_required
def delete_project(request, id):
    if request.method == "POST":
        project = Project.objects.get(id=id)
        project.delete()
        return redirect("list_projects")
    return render(request, "projects/delete.html")


@login_required
def edit_project(request, id):
    project = Project.objects.get(id=id)
    if request.method == "POST":
        form = ProjectForm(request.POST, instance=project)
        if form.is_valid():
            project = form.save(False)
            request.assignee = request.user
            project.save()
            return redirect("projects/detail.html")
    else:
        form = ProjectForm(instance=project)
    context = {
        "form": form,
    }
    return render(request, "projects/edit.html", context)
